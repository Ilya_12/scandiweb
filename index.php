<?php
	/**
	  *  This is a main file of application
	  *  It handles all incoming requests
	  *  Sends queries to database. 
	  *  Prepares answers
	  *  And sends them to client
	  *
	  */
	  
	include("account.php");
	include("lib/mysql.php");
	include("lib/page.php");
	
	//handling ajax request when we choose type of product on add page
	//loading html file according to name in type variable and sending content to client
	if($_POST[type]){
		$page = new Page("pages/".$_POST[type].".html");
		echo $page;
		return;
	}
	
	//creating connection to database
	$connection = new Connection($dbconn);
	
	//Quering SKU numbers from database that equal to pointed in ajax request
	//If found sending a message that given SKU is busy
	if($_POST[skunumber]){
		$products = $connection->data_request("SELECT * FROM Products WHERE sku='$_POST[skunumber]'");
		if(count($products) > 0) echo true;
		return;
	}
	
	//Remove product from database
	if($_POST[items]){
		$connection->data_change("DELETE FROM Products WHERE id IN(".join(",",$_POST[items]).")");
	}
	
	//Adding provided product to database by creating an instance depending on it's type
	if($_POST[product]){
		switch($_POST[option]){
			case "DVD": $product = new Dvd($_POST[sku], $_POST[name], $_POST[price], $_POST[size]); break;
			case "Book": $product = new Book($_POST[sku], $_POST[name], $_POST[price], $_POST[weight]); break;
			case "Furniture": $product = new Furniture($_POST[sku], $_POST[name], $_POST[price], $_POST[length], $_POST[width], $_POST[height]); break;
		}
		
		$connection->data_change("INSERT INTO Products (".$product->getColumns().") VALUES (".$product->getValues().")");
	}
	
	//Loading main page or going to product adding page if user pressed appropriate button
	if($_POST[add])$page = new Page("addProduct.html");
	else{
		$page = new Page("productList.html");
		$products = $connection->data_request("SELECT * FROM Products");
		$page->replace("content",join($products));
	} 
	
	echo $page;
?>