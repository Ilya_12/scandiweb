<?php
	/**
	  *  This class represent common fields  and functions for all products
	  *
	  */
	class Product implements IProduct{
		
		protected $id;
		protected $sku;
		protected $name;
		protected $price;
		
		function __construct($sku, $name, $price){
			$this->sku = $sku;
			$this->name = $name;
			$this->price = $price;
		}
		
		//Additionally to constructor this function gives a possibility to add ID to instance
		function insertId($id){
			$this->id = $id;
		}
		//Gives values to SQL
		function getValues(){
			return "$this->sku,$this->name,$this->price";
		}
		//Gives column names to SQL
		function getColumns(){
			return "sku,name,price";
		}
		//Loading template and placing values from instance to appropriate key
		//Thus converting instance when calling him
		function __toString(){
			$html = new Page("pages/product.html");
			$html->replace("id",$this->id);
			$html->replace("sku",$this->sku);
			$html->replace("name",$this->name);
			$html->replace("price","$this->price $");
			
			return $html;
		}
	}
?>