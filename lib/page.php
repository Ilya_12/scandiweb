<?php
	/**
	  *  This class represents a template
	  *  
	  *  when creating an instance loading an html code from file
	  *  
	  *  a template has special tags <key:name_of_label> that will be replaced to corresponding value 
	  */
	class Page{
		private $template;
		
		function __construct($filename)
		{
			$file = fopen($filename,"r");
			$this->template = fread($file, filesize($filename));
			fclose($file);
		}
		
		//changes key to value
		function replace($key, $val)
		{
			$this->template = str_replace ("<key:$key>", $val, $this->template);	
		}
		
		function __toString()
		{
			/**
			  *  outputs ready html page
			  */
			return $this->template;
		}
	}
?>