<?php
	class Dvd extends Product implements IProduct{
		const TYPE=1;
		private $size;
		
		function __construct($sku,$name,$price,$size){
			parent::__construct($sku,$name,$price);
			$this->size=$size;
		}
		function getValues(){
			return "'$this->sku','$this->name',$this->price,".self::TYPE.",$this->size";
		}
		function getColumns(){
			return "sku,name,price,type,size";
		}
		function __toString(){
			$html=parent::__toString();
			$html->replace("description","Size: $this->size MB");
			
			return "".$html;
		}
	}
?>