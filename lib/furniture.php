<?php
	class Furniture extends Product implements IProduct{
		const TYPE=3;
		private $length;
		private $width;
		private $height;
		
		function __construct($sku,$name,$price,$length,$width,$height){
			parent::__construct($sku,$name,$price);
			$this->length=$length;
			$this->width=$width;
			$this->height=$height;
		}
		function getValues(){
			return "'$this->sku','$this->name',$this->price,".self::TYPE.",$this->length,$this->width,$this->height";
		}
		function getColumns(){
			return "sku,name,price,type,length,width,height";
		}
		function __toString(){
			$html=parent::__toString();
			$html->replace("description","Dimension: ".$this->length."x".$this->width."x".$this->height);
			
			return "".$html;
		}
	}
?>