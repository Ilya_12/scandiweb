<?php
	/**
	  *  This class provides a connection to database
	  *  
	  *  it has only one field that represents connection
	  *  static member allows us to have only one connection
	  *  
	  *  constructor receives an associative array with connection properties
	  *  and makes a connection to database
	  */
	
	include("iproduct.php");
	include("product.php");
	include("dvd.php");
	include("book.php");
	include("furniture.php");
	
	class Connection
	{
		private static $connection;
		
		function __construct($dbconn)
		{
			$connection = mysql_connect($dbconn['host'], $dbconn['user'], $dbconn['pass']);
			mysql_select_db($dbconn['db']);
			self::$connection = $connection;
		}
		
		function data_request($sql)
		{
			/**
			  *  executing an sql query with SELECT clause
			  *  creating an instance based on type of product
			  *  placing instance to array
			  */
			$result = mysql_query($sql, self::$connection);
			$data = array();
			
			while($row = @mysql_fetch_array($result, MYSQL_ASSOC))
			{
				switch($row[type]){
					case 1: $instance = new Dvd($row[sku], $row[name], $row[price], $row[size]); break;
					case 2: $instance = new Book($row[sku], $row[name], $row[price], $row[weight]); break;
					case 3: $instance = new Furniture($row[sku], $row[name], $row[price], $row[length], $row[width], $row[height]); break;
				}
				$instance->insertId($row[id]);
				$data[] = $instance;
			}
			return $data;
		}
		
		function data_change($sql)
		{
			/**
			  *  executing an sql query with INSERT, UPDATE or DELETE clause
			  *  and returning a number of rows changed or inserted
			  */
			$result = mysql_query($sql, self::$connection);
			return mysql_affected_rows();
		}
	}
?>