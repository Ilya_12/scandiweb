<?php
	class Book extends Product implements IProduct{
		const TYPE=2;
		private $weight;
		
		function __construct($sku,$name,$price,$weight){
			parent::__construct($sku,$name,$price);
			$this->weight=$weight;
		}
		function getValues(){
			return "'$this->sku','$this->name',$this->price,".self::TYPE.",$this->weight";
		}
		function getColumns(){
			return "sku,name,price,type,weight";
		}
		function __toString(){
			$html=parent::__toString();
			$html->replace("description","Weight: $this->weight KG");
			
			return "".$html;
		}
	}
?>